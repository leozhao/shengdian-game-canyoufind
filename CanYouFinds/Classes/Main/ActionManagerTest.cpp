#include "ActionManagerTest.h"
#include "../testResource.h"
#include "cocos2d.h"

enum
{
	kTagNode,
	kTagGrossini,
	kTagSequence,
};

Layer* nextActionManagerAction();
Layer* backActionManagerAction();
Layer* restartActionManagerAction();

static int sceneIdx = -1;

#define MAX_LAYER    2

Layer* createActionManagerLayer(int nIndex)
{
	switch (nIndex)
	{
	case 0: return new CrashTest();
	case 1: return new LogicTest();
	}

	return NULL;
}

Layer* nextActionManagerAction()
{
	sceneIdx++;
	sceneIdx = sceneIdx % MAX_LAYER;

	auto layer = createActionManagerLayer(sceneIdx);
	layer->autorelease();

	return layer;
}

Layer* backActionManagerAction()
{
	sceneIdx--;
	int total = MAX_LAYER;
	if (sceneIdx < 0)
		sceneIdx += total;

	auto layer = createActionManagerLayer(sceneIdx);
	layer->autorelease();

	return layer;
}

Layer* restartActionManagerAction()
{
	auto layer = createActionManagerLayer(sceneIdx);
	layer->autorelease();

	return layer;
}

//------------------------------------------------------------------
//
// ActionManagerTest
//
//------------------------------------------------------------------

ActionManagerTest::ActionManagerTest(void)
{
}

ActionManagerTest::~ActionManagerTest(void)
{
}

std::string ActionManagerTest::title() const
{
	return "ActionManager Test";
}
std::string ActionManagerTest::subtitle() const
{
	return "No title";
}
void ActionManagerTest::restartCallback(Ref* sender)
{
	auto s = new ActionManagerTestScene();
	s->addChild(restartActionManagerAction());

	Director::getInstance()->replaceScene(s);
	s->release();
}

void ActionManagerTest::nextCallback(Ref* sender)
{
	auto s = new ActionManagerTestScene();
	s->addChild(nextActionManagerAction());
	Director::getInstance()->replaceScene(s);
	s->release();
}

void ActionManagerTest::backCallback(Ref* sender)
{
	auto s = new ActionManagerTestScene();
	s->addChild(backActionManagerAction());
	Director::getInstance()->replaceScene(s);
	s->release();
}

//------------------------------------------------------------------
//
// Test1
//
//------------------------------------------------------------------

void CrashTest::onEnter()
{
	ActionManagerTest::onEnter();

	auto child = Sprite::create(s_pathGrossini);
	child->setPosition(VisibleRect::center());
	addChild(child, 1);

	// background
	Size winSize = Director::getInstance()->getWinSize();
	auto background = Sprite::create("Images/background.png");
	background->setAnchorPoint(Point(0, 1));
	background->setPosition(Point(0, winSize.height));
	addChild(background);

	//Sum of all action's duration is 1.5 second.
	child->runAction(RotateBy::create(1.5f, 90));
	child->runAction(Sequence::create(
		DelayTime::create(1.4f),
		FadeOut::create(1.1f),
		NULL)
		);

	//After 1.5 second, self will be removed.
	runAction(Sequence::create(
		DelayTime::create(1.4f),
		CallFunc::create(CC_CALLBACK_0(CrashTest::removeThis, this)),
		NULL)
		);
}

void CrashTest::removeThis()
{
	_parent->removeChild(this, true);

	nextCallback(this);
}

std::string CrashTest::subtitle() const
{
	return "Test 1. Should not crash";
}

//------------------------------------------------------------------
//
// Test2
//
//------------------------------------------------------------------
void LogicTest::onEnter()
{
	ActionManagerTest::onEnter();

	auto grossini = Sprite::create(s_pathGrossini);
	addChild(grossini, 0, 2);
	grossini->setPosition(VisibleRect::center());
	// background
	Size winSize = Director::getInstance()->getWinSize();
	auto background = Sprite::create("Images/background3.jpg");
	background->setAnchorPoint(Point(0, 1));
	background->setPosition(Point(0, winSize.height));
	addChild(background);
	grossini->runAction(Sequence::create(
		MoveBy::create(1, Point(150, 0)),
		CallFuncN::create(CC_CALLBACK_1(LogicTest::bugMe, this)),
		NULL)
		);
}

void LogicTest::bugMe(Node* node)
{
	node->stopAllActions(); //After this stop next action not working, if remove this stop everything is working
	node->runAction(ScaleTo::create(2, 2));
}

std::string LogicTest::subtitle() const
{
	return "Logic test";
}

//------------------------------------------------------------------
//
// ActionManagerTestScene
//
//------------------------------------------------------------------
void ActionManagerTestScene::runThisTest()
{
	auto layer = nextActionManagerAction();
	addChild(layer);

	Director::getInstance()->replaceScene(this);
}
